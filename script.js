db.users.insertMany(
	[
		{
			"firstName": "Diane",
			"lastName": "Murphy",
			"Email": "dmurphy@mail.com",
			"isAdmin?": "no",
			"isActive?": "yes"
		},
		{
			"firstName": "Mary",
			"lastName": "Patterson",
			"Email": "mpatterson@mail.com",
			"isAdmin?": "no",
			"isActive?": "yes"
		},
		{
			"firstName": "Jeff",
			"lastName": "Firrelli",
			"Email": "jfirrelli@mail.com",
			"isAdmin?": "no",
			"isActive?": "yes",
		},
		{
			"firstName": "Gerard",
			"lastName": "Bondur",
			"Email": "gbondur@mail.com",
			"isAdmin?": "no",
			"isActive?": "yes"
		},
		{
			"firstName": "Pamela",
			"lastName": "Castillo",
			"Email": "pcastillo@mail.com",
			"isAdmin?": "yes",
			"isActive?": "no"
		},
		{
			"firstName": "George",
			"lastName": "Vanauf",
			"Email": "gvanauf@mail.com",
			"isAdmin?": "yes",
			"isActive?": "yes"
		}
	]
);


db.courses.insertMany(
	[
		{
			"Name": "Professional Development",
			"Price": "10,000"
		},
		{
			"Name": "Business Processing",
			"Price": "13,000"
		}
	]
)

db.courses.updateOne(
	{
		"_id" : ObjectId("6125b6c21ffc5e88402756b9"),
		"Name": "Professional Development"
	},
	{
		$set: {"Enrollees": "Murphy, Firrelli"}
	}
)

db.courses.updateOne(
	{
		"_id" : ObjectId("6125b6c21ffc5e88402756ba"),
		"Name" : "Business Processing"
	},
	{
		$set: {"Enrollees": "Bondur, Patterson"}
	}
)

db.users.find(
{
	"isAdmin?" : "no",
})

